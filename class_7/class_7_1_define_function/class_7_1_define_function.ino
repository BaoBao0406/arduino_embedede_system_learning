float ans;

float cirArea(int r) {
  float area = 3.14 * r * r;
  return area;
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  ans = cirArea(10);
  Serial.println(ans);
  ans = cirArea(20);
  Serial.println(ans);
}

void loop() {
  // put your main code here, to run repeatedly:

}
