#include <SPI.h>

byte symbol[8] = {0x60, 0xF0, 0xF0, 0x7F, 0x07, 0x06, 0x0C, 0x08};

const byte CS = 10;

const byte NOOP = 0x0;
const byte DECODEMODE = 0x9;
const byte INTENSITY = 0xA;
const byte SCANLIMIT = 0xB;
const byte SHUTDOWN = 0xC;
const byte DISPLAYTEST = 0xF;

void max7219(byte reg, byte data) {
  digitalWrite(CS, LOW);
  SPI.transfer(reg);
  SPI.transfer(data);
  digitalWrite(CS, HIGH);
}

void setup() {
  // put your setup code here, to run once:
  pinMode(SS, OUTPUT);
  pinMode(CS, OUTPUT);

  digitalWrite(CS, HIGH);
  SPI.begin();

  max7219(SCANLIMIT, 7);
  max7219(DECODEMODE, 0);
  max7219(INTENSITY, 8);
  max7219(DISPLAYTEST, 0);
  max7219(SHUTDOWN, 1);

  for (byte i=0; i<8; i++) {
    max7219(i+1, 0);
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  for (byte i=0; i<8; i++) {
    max7219(i+1, symbol[i]);
  }
}
