#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);

byte sp[6][8] = {
  {B00100, B01110, B11111, B10101, B11111, B01110, B01010, B10001},
  {B00100, B01110, B11111, B11010, B11111, B00100, B01010, B01010},
  {B00100, B01110, B11111, B11110, B11111, B01110, B00100, B00100},
  {B00100, B01110, B11111, B11111, B11111, B00100, B01010, B01010},
  {B00100, B01110, B11111, B01111, B11111, B01110, B00100, B00100},
  {B00100, B01110, B11111, B01101, B11111, B00100, B01010, B01010}
};

byte index = 0;

void setup() {
  // put your setup code here, to run once:
  lcd.init();
  lcd.backlight();
  
  for (byte i=0; i<6; i++) {
    lcd.createChar(i, sp[i]);
  }

  lcd.home();
  lcd.print("Invader");
}

void loop() {
  // put your main code here, to run repeatedly:
  lcd.setCursor(8, 0);
  lcd.write(index);

  if (++index > 5) {
    index = 0;
  }

  delay(300);
}
