#include <LiquidCrystal.h>

LiquidCrystal lcd(11, 12, 6, 5, 4, 3);


void setup() {
  // put your setup code here, to run once:
  lcd.begin(16, 2);
  lcd.print("hello, world!");
}

void loop() {
  // put your main code here, to run repeatedly:
  lcd.setCursor(0, 1);
  lcd.print(millis()/1000);
}
