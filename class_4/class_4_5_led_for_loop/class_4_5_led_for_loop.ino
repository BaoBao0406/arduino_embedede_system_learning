const byte startPin = 8;
const byte endPin = 12;
byte lightPin = startPin;

void setup() {
  // put your setup code here, to run once:
  for (byte i = startPin; i <= endPin; i++) {
    pinMode(i, OUTPUT);
    digitalWrite(i, LOW);
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(lightPin, HIGH);
  delay(100);
  digitalWrite(lightPin, LOW);

  if (lightPin < endPin) {
    lightPin ++;
  } else {
    lightPin = startPin;
  }
}
