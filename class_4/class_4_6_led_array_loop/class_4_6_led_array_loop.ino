const byte LEDs[] = {8, 9, 10, 11, 12};
const byte total = sizeof(LEDs);

void setup() {
  // put your setup code here, to run once:
  for (byte i = 0; i < total; i ++) {
    pinMode(LEDs[i], OUTPUT);
    digitalWrite(LEDs[i], LOW);
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  for (byte i = 0; i < total - 1; i++) {
    digitalWrite(LEDs[i], HIGH);
    delay(100);
    digitalWrite(LEDs[i], LOW);
  }

  for (byte i = total - 1; i > 0; i -= 1) {
    digitalWrite(LEDs[i], HIGH);
    delay(100);
    digitalWrite(LEDs[i], LOW);
  }
}
